import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import CartItem from './CartItem'
import { openModal } from '../features/modal/modalSlice'

const CartContainer = () => {

    const dispatch = useDispatch()

    const { cartItems, total, amount, isLoading } = useSelector(store => store.cart)

    if (cartItems.length < 1 && !isLoading) {
        return (
            <section className='cart'>
                <header>
                    <h2>YOUR BAG</h2>
                    <h4 className="empty-cart">Your cart is empty</h4>
                </header>
            </section>
        )
    }

    return (
        <section className='cart'>
            <header>
                <h2>YOUR BAG</h2>
            </header>
            <div>
                {
                    cartItems.length == 0 && isLoading ?
                        <div className="cart">
                            <header>
                                <h4>Loading....</h4>
                            </header>
                        </div> :
                        cartItems.map(item => <CartItem key={item.id} {...item} />)
                }
            </div>
            <footer>
                <hr />
                <div className='cart-total'>
                    <h4>
                        total <span>$ {total.toFixed(2)}</span>
                    </h4>
                </div>
                <button className='btn clear-btn' onClick={() => dispatch(openModal())}>CLEAR CART</button>
            </footer>
        </section>
    )
}

export default CartContainer