import Navbar from "./components/Navbar";
import CartContainer from "./components/CartContainer";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { calculateTotal, getCartItems } from "./features/cart/cartSlice";
import Modal from "./components/Modal";

function App() {

  const { cartItems } = useSelector(store => store.cart)

  const { isOpen } = useSelector(store => store.modal)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getCartItems('userName or something'))
    // .then(res => console.log(res))
  }, [])

  useEffect(() => {
    let amountAll = 0
    cartItems.map(item => {
      amountAll += item.amount * item.price
    })
    dispatch(calculateTotal(amountAll))

  }, [cartItems])

  return (
    <main>
      {isOpen && <Modal />}
      <Navbar />
      <CartContainer />
    </main>
  )
}
export default App;
