import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    cartItems: [],
    amount: 4,
    total: 0,
    isLoading: false
}

const url = 'https://course-api.com/react-useReducer-cart-project'

// export const getCartItems = createAsyncThunk('cart/getCartItems', () => {
//     return fetch(url)
//         .then(res => res.json())
//         .catch(err => console.log(err))
// })

export const getCartItems = createAsyncThunk('cart/getCartItems',
    async (name, thunkAPI) => {
        try {
            // console.log(name)
            const response = await axios(url)
            return response.data
        } catch (err) {
            thunkAPI.rejectWithValue('Something went wrong !')
        }
    })


const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        clearCart: (state) => {
            state.cartItems = []
        },
        removeItem: (state, { payload }) => {
            const filterItems = state.cartItems.filter(item => item.id !== payload)
            state.cartItems = filterItems
        },
        increaseItem: (state, action) => {
            const item = state.cartItems.find(item => item.id === action.payload)
            item.amount = item.amount + 1
            state.amount = state.amount + 1
        },
        decreaseItem: (state, action) => {
            const item = state.cartItems.find(item => item.id === action.payload)
            item.amount = item.amount - 1
            state.amount = state.amount - 1
        },
        calculateTotal: (state, { payload }) => {
            state.total = payload
        }
    },
    extraReducers: {
        [getCartItems.pending]: (state) => {
            state.isLoading = true
        },
        [getCartItems.fulfilled]: (state, action) => {
            state.isLoading = false
            state.cartItems = action.payload
        },
        [getCartItems.rejected]: (state) => {
            state.isLoading = false
        },
    },
})

// console.log(cartSlice)

export const { clearCart, increaseItem, removeItem,
    decreaseItem, calculateTotal } = cartSlice.actions

export default cartSlice.reducer